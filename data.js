

module.exports = {
    getPerson,
    getPersonByLisncePlate
}

function getPerson(id) {
    let result = people.filter((p) => p.ID_NUMBER == id);

    if (!result || result.length == 0) {
        let err = new Error("Could not find person with");
        err.status = 404;
        throw err;
    }

    if (result.length > 1) {
        let err = new Error("there are more then one person with that id");
        err.status = 500;
        throw err;
    }

    return result[0];

}

function getPersonByLisncePlate(licensePlate) {
    let result = people.filter((p) => p.LICENSE_PLATE_NMBER.includes(licensePlate));

    if (!result || result.length == 0) {
        let err = new Error("Could not find person with that licence plate");
        err.status = 404;
        throw err;
    }

    if (result.length > 1) {
        let err = new Error("there are more then one person with that lisence plate");
        err.status = 500;
        throw err;
    }

    return result[0];
}

let people = [
    {
        "ENTITY_TYPE": "Soldier",
        "FIRST_NAME": "Tomer",
        "ID_NUMBER": "313598617",
        "LAST_NAME": "Buhadana",
        "USER_NAME": "712tomerb",
        "RANK": "\"Samar pazam\"",
        "SERVICE_TYPE": "Post",
        "BIRTH_DATE": "1991-01-01",
        "SEX": "m",
        "RELEASE_DATE": "2017-02-07",
        "ADDRESS": "Room 601",
        "TAFKID": "Developer",
        "VOIP_PHONE": "7122",
        "CELL_PHONE": "0523525845",
        "IMAGE_URL": "/images/buga.jpg",
        "LICENSE_PLATE_NMBER": "8866666"
    },
    {
        "ENTITY_TYPE": "Soldier",
        "FIRST_NAME": "Roy",
        "ID_NUMBER": "315341958",
        "LAST_NAME": "Mor",
        "USER_NAME": "711roym",
        "RANK": "\"noob\"",
        "SERVICE_TYPE": "Post",
        "BIRTH_DATE": "1991-01-01",
        "SEX": "m",
        "RELEASE_DATE": "2017-02-07",
        "ADDRESS": "Pitoosia",
        "TAFKID": "Developer",
        "VOIP_PHONE": "Classified",
        "CELL_PHONE": "0547550722",
        "IMAGE_URL": "/images/roymor.jpg",
        "LICENSE_PLATE_NMBER": "6866866"
    }, {
        "ENTITY_TYPE": "Soldier",
        "FIRST_NAME": "Dori",
        "ID_NUMBER": "207903550",
        "LAST_NAME": "Aviram",
        "USER_NAME": "712doria",
        "RANK": "\"noob\"",
        "SERVICE_TYPE": "Post",
        "BIRTH_DATE": "1991-01-01",
        "SEX": "m",
        "RELEASE_DATE": "2017-02-07",
        "ADDRESS": "Pitoosia",
        "TAFKID": "Developer",
        "VOIP_PHONE": "Classified",
        "CELL_PHONE": "0503345660",
        "IMAGE_URL": "/images/dori.jpg",
        "LICENSE_PLATE_NMBER": "2952165"
    }, {
        "ENTITY_TYPE": "Soldier",
        "FIRST_NAME": "Dan",
        "ID_NUMBER": "316592302",
        "LAST_NAME": "Sela",
        "USER_NAME": "722dans",
        "RANK": "\"noob\"",
        "SERVICE_TYPE": "Post",
        "BIRTH_DATE": "1991-01-01",
        "SEX": "m",
        "RELEASE_DATE": "2017-02-07",
        "ADDRESS": "Pitoosia",
        "TAFKID": "Developer",
        "VOIP_PHONE": "Classified",
        "CELL_PHONE": "0545872423",
        "IMAGE_URL": "/images/dan.jpg",
        "LICENSE_PLATE_NMBER": "01234589"
    }, {
        "ENTITY_TYPE": "Soldier",
        "FIRST_NAME": "Daniel",
        "ID_NUMBER": "316582964",
        "LAST_NAME": "Amar",
        "USER_NAME": "722daniela",
        "RANK": "\"noob\"",
        "SERVICE_TYPE": "Post",
        "BIRTH_DATE": "1991-01-01",
        "SEX": "m",
        "RELEASE_DATE": "2017-02-07",
        "ADDRESS": "Techni",
        "TAFKID": "Developer",
        "VOIP_PHONE": "Classified",
        "CELL_PHONE": "0524643305",
        "IMAGE_URL": "/images/amar.jpg",
        "LICENSE_PLATE_NMBER": "1797538"
    }
];