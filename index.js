


const express = require('express');
const dataService = require('./data');


let app = express();

app.use("/images", express.static("images"));

app.get("/person/:id", (req, res, next) => {
    let person = dataService.getPerson(req.params.id);
    res.status(200).json(person);
})

app.get("/licensePlate/:licensePlate", (req, res, next) => {
    let person = dataService.getPersonByLisncePlate(req.params.licensePlate);
    res.status(200).json(person);
})


app.use((req, res, next) => {
    let error = new Error("Page Not Found");
    error.status = 404;
    next(error);
})

app.use((err, req, res, next) => {
    let status = err.status || 500;
    res.status(status).json({
        message: err.message,
        status,
        stack: err.stack
    });
})


let port = process.env.PORT || 3000;

app.listen(port, () => {
    console.log(`listening on port ${port}`);
})